/* eslint-disable eqeqeq */
import PropTypes from 'prop-types'
import React,{useState,useEffect} from 'react'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';

import {
  Container,
  Header,
} from 'semantic-ui-react'
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%',
    maxWidth: '100ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  img :{
    width: '70%',
    height: '77%'  
  }
}));

const URL = 'https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/IT/EUR/it-IT/TRN-sky/CTA-sky/anytime';



const options = {
  method: 'GET',
  url: URL,
  params: {inboundpartialdate: new Date().toISOString().split('T')[0]},
  headers: {
    'x-rapidapi-key': '344bd8390cmshdff44f17e96fdaap130367jsn79b1b0a555de',
    'x-rapidapi-host': 'skyscanner-skyscanner-flight-search-v1.p.rapidapi.com'
  }
};



const HomepageHeading = ({ mobile }) => {
  const [result, setResult] = useState({});



  
  const classes = useStyles();

  useEffect(() => {
    axios.request(options).then(res => setResult(res.data));
    
},[]);

  useEffect(() => {
    console.log(result)
  }, [result])
 
  console.log(result)
  return <Container text>
    <Header
      as='h1'
      content='Tracking Flight'
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <TextField id="standard-search" label="Partenza" type="search" />
        <TextField id="standard-search" label="Arrivo" type="search" />
      </div>
    </form>
    <List className={classes.root}>
      {result?.Quotes?.map(item =>(
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar  src={'/images/'+item?.OutboundLeg?.CarrierIds[0]+'.png'} />
        </ListItemAvatar>
        <ListItemText
         style={{color: "black",textAlign: 'center'}}
         
          // eslint-disable-next-line eqeqeq
          primary={result?.Carriers?.filter(fly => fly?.CarrierId === item?.OutboundLeg?.CarrierIds[0]).map(flyfilter => {return flyfilter?.Name?.toString()})}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                {item?.MinPrice+ ' €   '}
              </Typography>
                -  {new Date(item?.OutboundLeg?.DepartureDate).toISOString().split('T')[0]}  - Partenza ---{ result?.Places?.filter(y => y?.PlaceId === item?.OutboundLeg?.OriginId).map(f => {return f?.Name?.toString()})} <br></br>
                Arrivo --- { result?.Places?.filter(y => y?.PlaceId === item?.OutboundLeg?.DestinationId).map(f => {return f?.Name?.toString()})}
              <Radio
                checked={item?.Direct === true}
                value="Diretto"
                label="Diretto"
                name="radio-button-demo"
              />
              Diretto
            </React.Fragment>
              
          }
        />
      </ListItem>
      ))}
      <Divider variant="inset" component="li" />
    </List>
  </Container>
}

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}


export default HomepageHeading;